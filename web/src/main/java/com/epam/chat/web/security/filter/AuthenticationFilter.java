package com.epam.chat.web.security.filter;

import com.epam.chat.model.entity.User;
import com.epam.chat.service.AuthService;
import com.epam.chat.web.security.filter.binding.Secured;
import com.sap.security.um.user.PersistenceException;
import com.sap.security.um.user.UnsupportedUserAttributeException;
import com.sap.security.um.user.UserProvider;

import javax.inject.Inject;
import javax.ws.rs.InternalServerErrorException;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.ext.Provider;
import java.io.IOException;
import java.util.Optional;

@Secured
@Provider
public class AuthenticationFilter implements ContainerRequestFilter {

    private static final String NOT_HAVE_ACCESS_MESSAGE = "You cannot access this resource";

    @Inject
    private AuthService authService;

    @Inject
    private UserProvider userProvider;

    @Override
    public void filter(ContainerRequestContext containerRequestContext) throws IOException {
        try {
            com.sap.security.um.user.User user = userProvider.getCurrentUser();
            String login = user.getAttribute("name");
            Optional<User> userInDB = authService.findByLogin(login);
            if (!userInDB.isPresent()) {
                throw new NotAuthorizedException(NOT_HAVE_ACCESS_MESSAGE);
            }
        } catch (PersistenceException | UnsupportedUserAttributeException e) {
            throw new InternalServerErrorException();
        }
    }
}
