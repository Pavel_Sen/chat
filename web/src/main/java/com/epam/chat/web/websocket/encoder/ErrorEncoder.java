package com.epam.chat.web.websocket.encoder;

import com.epam.chat.model.dto.response.ErrorDto;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.websocket.EncodeException;
import javax.websocket.Encoder;
import javax.websocket.EndpointConfig;
import javax.ws.rs.InternalServerErrorException;

public class ErrorEncoder implements Encoder.Text<ErrorDto> {
    private static final String ENCODE_MESSAGE_ERROR = "Error encoding message";

    @Override
    public String encode(ErrorDto errorDto) throws EncodeException {
        try {
            return new ObjectMapper().writeValueAsString(errorDto);
        } catch (JsonProcessingException e) {
            throw new InternalServerErrorException(ENCODE_MESSAGE_ERROR);
        }
    }

    @Override
    public void init(EndpointConfig endpointConfig) {

    }

    @Override
    public void destroy() {

    }
}
