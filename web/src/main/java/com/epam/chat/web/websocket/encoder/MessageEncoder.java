package com.epam.chat.web.websocket.encoder;

import com.epam.chat.model.dto.response.MessageDto;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.websocket.*;
import javax.ws.rs.InternalServerErrorException;

public class MessageEncoder implements Encoder.Text<MessageDto>{
    private static final String ERROR_ENCODING_ERRORDTO = "Error encoding ErrorDto";

    @Override
    public String encode(MessageDto messageDto) throws EncodeException {
        try {
            return new ObjectMapper().writeValueAsString(messageDto);
        } catch (JsonProcessingException e) {
            throw new InternalServerErrorException(ERROR_ENCODING_ERRORDTO);
        }
    }

    @Override
    public void init(EndpointConfig endpointConfig) {

    }

    @Override
    public void destroy() {

    }
}
