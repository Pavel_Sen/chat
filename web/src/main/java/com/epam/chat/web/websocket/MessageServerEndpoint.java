package com.epam.chat.web.websocket;

import com.epam.chat.model.dto.response.ErrorDto;
import com.epam.chat.model.dto.response.MessageDto;
import com.epam.chat.web.websocket.encoder.ErrorEncoder;
import com.epam.chat.web.websocket.encoder.MessageEncoder;
import com.epam.chat.web.websocket.exception.NotLoggedInException;
import lombok.EqualsAndHashCode;

import javax.websocket.*;
import javax.websocket.server.ServerEndpoint;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.security.Principal;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

@ServerEndpoint(value = "/messages",
                encoders = {MessageEncoder.class, ErrorEncoder.class})
@EqualsAndHashCode
public class MessageServerEndpoint {
    private static final String NOT_LOGGED = "You are not logged in";

    @EqualsAndHashCode.Exclude
    private Session session;
    private static Set<MessageServerEndpoint> endpoints = new ConcurrentHashMap<Endpoint, Integer>().newKeySet();
    private String userLogin;

    @OnOpen
    public void onOpen(Session session) throws IOException, NotLoggedInException {
        if (session.getUserPrincipal() == null) {
            throw new NotLoggedInException(NOT_LOGGED);
        }
        Principal principal = session.getUserPrincipal();
        userLogin = principal.getName();
        this.session = session;
        endpoints.add(this);
    }

    @OnError
    public void onError(Session session, Throwable throwable) {
        try {
            Response.StatusType type = getStatusType(throwable);
            ErrorDto errorDto = new ErrorDto();
            errorDto.setCode(type.getStatusCode());
            errorDto.setMessage(type.getReasonPhrase());
            errorDto.setDescription(throwable.getLocalizedMessage());
            session.getBasicRemote().sendObject(errorDto);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (EncodeException e) {
            e.printStackTrace();
        }
    }

    @OnClose
    public void onClose() {
        endpoints.remove(this);
    }

    public void sendMessage(List<String> userNames, MessageDto message) {
        for (MessageServerEndpoint endpoint : endpoints) {
            if (userNames.contains(endpoint.userLogin)) {
                try {
                    endpoint.session.getBasicRemote().sendObject(message);
                } catch (IOException e) {
                    onError(endpoint.session, e);
                } catch (EncodeException e) {
                    onError(endpoint.session, e);
                }
            }
        }
    }

    private Response.StatusType getStatusType(Throwable ex) {
        if (ex instanceof WebApplicationException) {
            return ((WebApplicationException) ex).getResponse().getStatusInfo();
        } else if (ex instanceof NotLoggedInException) {
           return Response.Status.UNAUTHORIZED;
        } else {
            return Response.Status.INTERNAL_SERVER_ERROR;
        }
    }

}
