package com.epam.chat.web.controller;

import com.epam.chat.model.dto.*;
import com.epam.chat.model.dto.response.AttachmentDto;
import com.epam.chat.model.dto.response.ChatDto;
import com.epam.chat.model.dto.response.MembershipDto;
import com.epam.chat.model.dto.response.MessageDto;
import com.epam.chat.service.AttachmentService;
import com.epam.chat.service.ChatService;
import com.epam.chat.service.MembershipService;
import com.epam.chat.service.MessageService;
import com.epam.chat.web.security.filter.binding.Secured;
import com.epam.chat.web.websocket.MessageServerEndpoint;
import com.sap.security.um.user.PersistenceException;
import com.sap.security.um.user.UnsupportedUserAttributeException;
import com.sap.security.um.user.User;
import com.sap.security.um.user.UserProvider;
import io.swagger.annotations.*;
import org.apache.chemistry.opencmis.commons.data.ContentStream;
import org.glassfish.jersey.media.multipart.FormDataBodyPart;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.stream.Collectors;

@Path("/chats")
@Api(value = "Chat")
@Secured
public class ChatController {

    private static final String FORBIDDEN_ERROR_MESSAGE = "Only admin can perform such actions";
    private static final String NO_MESSAGE_IN_CHAT = "There's no message wit such id in this chat";
    private static final String CHAT_CREATED_MESSAGE = "Chat has been created";
    private static final String CHAT_UPDATED_MESSAGE = "Chat has been updated";
    private static final String CHAT_CREATED = "/chats/%d";
    private static final String USER_NAME_ATTRIBUTE = "name";
    private static final String MESSAGE_CREATED = "/messages/%d";

    private ChatService chatService;
    private MembershipService membershipService;
    private MessageService messageService;
    private AttachmentService attachmentService;

    private UserProvider userProvider;

    private MessageServerEndpoint messageServerEndpoint;

    @Inject
    public ChatController(ChatService chatService, MembershipService membershipService, MessageService messageService,
                          AttachmentService attachmentService, UserProvider userProvider) {
        this.chatService = chatService;
        this.membershipService = membershipService;
        this.messageService = messageService;
        this.attachmentService = attachmentService;
        this.userProvider = userProvider;
        messageServerEndpoint = new MessageServerEndpoint();
    }

    @ApiOperation(value = "Find chat by id", response = ChatDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully found chat by id"),
            @ApiResponse(code = 404, message = "Chat with such id doesn't exist")
    })
    @GET
    @Path("/{id}")
    @Produces({MediaType.APPLICATION_JSON})
    public Response findChatById(@ApiParam(value = "Id to find chat", required = true) @PathParam("id") long id) {
        ChatDto chat = chatService.findById(id);
        return Response.ok(chat).build();
    }

    @ApiOperation(value = "Find chat messages by chat id", response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully found chat messages"),
            @ApiResponse(code = 404, message = "Chat with such id doesn't exist"),
            @ApiResponse(code = 403, message = "Forbidden. Only chat members can see chat messages")

    })
    @GET
    @Path("/{id}/messages")
    @Produces({MediaType.APPLICATION_JSON})
    public Response findChatMessages(@PathParam("id") Long id,
                                     @QueryParam("limit") @Min(0) Long limit,
                                     @QueryParam("offset") @Min(0) @DefaultValue("0") Integer offset) throws PersistenceException, UnsupportedUserAttributeException {
        User user = userProvider.getCurrentUser();
        String currentUserLogin = user.getAttribute(USER_NAME_ATTRIBUTE);
        if (!membershipService.isUserChatMember(id, currentUserLogin)) {
            throw new ForbiddenException(FORBIDDEN_ERROR_MESSAGE);
        }

        Long fieldsCount = messageService.getMessagesCountByChatId(id);
        if (limit == null) {
            limit = fieldsCount;
        }

        List<MessageDto> chatMessages = messageService.findMessagesByChatIdWithPagination(id, limit, offset);
        return Response
                .ok(chatMessages)
                .header("X-Total-Count", fieldsCount)
                .build();
    }

    @ApiOperation(value = "Find all chats", response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully found all chats")
    })
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public Response findAllChats() {
        List<ChatDto> allChats = chatService.findAll();
        return Response.ok(allChats).build();
    }

    @ApiOperation(value = "Add new chat", response = ChatDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Successfully added chat"),
            @ApiResponse(code = 400, message = "Invalid chat fields")
    })
    @POST
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response addChat(@ApiParam(value = "Chat object to saveContent in database", required = true) @Valid ChatCreateOrUpdateDto chat) throws URISyntaxException, PersistenceException, UnsupportedUserAttributeException {
        User user = userProvider.getCurrentUser();
        String currentUserLogin = user.getAttribute(USER_NAME_ATTRIBUTE);
        ChatDto createdChat = chatService.save(chat, currentUserLogin);
        messageService.saveEvent(createdChat.getId(), CHAT_CREATED_MESSAGE);
        String path = String.format(CHAT_CREATED, createdChat.getId());
        URI uri = new URI(path);
        return Response.created(uri).entity(createdChat).build();
    }


    @ApiOperation("Delete chat")
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "Successfully deleted chat"),
            @ApiResponse(code = 404, message = "Chat with such id doesn't exist"),
            @ApiResponse(code = 403, message = "Forbidden. Only chat members can delete chats")
    })
    @DELETE
    @Path("/{id}")
    @Produces({MediaType.APPLICATION_JSON})
    public Response deleteChat(@ApiParam(value = "Id to delete chat", required = true) @PathParam("id") long id) throws PersistenceException, UnsupportedUserAttributeException {
        User user = userProvider.getCurrentUser();
        String currentUserLogin = user.getAttribute(USER_NAME_ATTRIBUTE);
        if (!chatService.isUserChatAdmin(id, currentUserLogin)) {
            throw new ForbiddenException(FORBIDDEN_ERROR_MESSAGE);
        }
        chatService.removeById(id);
        return Response.noContent().build();
    }

    @ApiOperation(value = "Update chat", response = ChatDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully updated chat"),
            @ApiResponse(code = 404, message = "Chat with such id doesn't exist"),
            @ApiResponse(code = 400, message = "Invalid chat fields"),
            @ApiResponse(code = 403, message = "Forbidden. Only admin can update chats")
    })
    @PUT
    @Path("/{id}")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response updateChat(@ApiParam(value = "Id to update chat", required = true) @PathParam("id") long id,
                               @ApiParam(value = "Chat object to update in database", required = true) @Valid ChatCreateOrUpdateDto chat) throws PersistenceException, UnsupportedUserAttributeException {
        User user = userProvider.getCurrentUser();
        String currentUserLogin = user.getAttribute(USER_NAME_ATTRIBUTE);
        if (!chatService.isUserChatAdmin(id, currentUserLogin)) {
            throw new ForbiddenException(FORBIDDEN_ERROR_MESSAGE);
        }
        ChatDto updatedChat = chatService.update(id, chat);
        messageService.saveEvent(updatedChat.getId(), CHAT_UPDATED_MESSAGE);
        return Response.ok(updatedChat).build();
    }


    @ApiOperation(value = "Add new message", response = MessageDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Successfully added message"),
            @ApiResponse(code = 400, message = "Invalid message fields values"),
            @ApiResponse(code = 403, message = "Not current logged in user in request body"),
            @ApiResponse(code = 403, message = "User is not member of chat")
    })
    @POST
    @Path("/{id}/messages")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response addMessage(@ApiParam(value = "Id to save message in chat", required = true) @PathParam("id") long id,
                               @ApiParam(value = "Message object to save object in database") @Valid MessageCreateOrUpdateDto message)
            throws URISyntaxException, UnsupportedUserAttributeException, PersistenceException {
        User user = userProvider.getCurrentUser();
        String currentUserLogin = user.getAttribute(USER_NAME_ATTRIBUTE);
        MessageDto createdMessage = messageService.saveMessage(id, currentUserLogin, message);
        String path = String.format(MESSAGE_CREATED, createdMessage.getId());
        URI uri = new URI(path);
        return Response.created(uri).entity(createdMessage).build();
    }

    @POST
    @Path("/{id}/messages/attachments")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.APPLICATION_JSON)
    public Response addAttachment(@PathParam("id") long id,
                                  @FormDataParam("file") InputStream  uploadedInputStream,
                                  @FormDataParam("file") FormDataContentDisposition fileDetail,
                                  @FormDataParam("file") FormDataBodyPart bodyPart) throws PersistenceException, UnsupportedUserAttributeException, URISyntaxException {
        User user = userProvider.getCurrentUser();
        String currentUserLogin = user.getAttribute(USER_NAME_ATTRIBUTE);
        String fileName = fileDetail.getFileName();
        String mediaType = bodyPart.getMediaType().getType();

        MessageDto createdMessage = messageService.saveAttachment(id, currentUserLogin, fileName);
        String documentName = defineAttachmentDocumentName(fileName, createdMessage.getId());
        String objectId = attachmentService.saveContent(mediaType, documentName, uploadedInputStream);
        attachmentService.save(objectId, createdMessage);

        String path = String.format(MESSAGE_CREATED, createdMessage.getId());
        URI uri = new URI(path);
        List<MembershipDto> membershipsThatShouldGetMessage = membershipService.findByChatId(id);
        List<String> userNamesThatShouldReceiveMessage = membershipsThatShouldGetMessage.stream().map(membershipDto -> membershipDto.getUser().getLogin())
                .collect(Collectors.toList());
        messageServerEndpoint.sendMessage(userNamesThatShouldReceiveMessage, createdMessage);
        return Response.created(uri).entity(createdMessage).build();
    }

    @GET
    @Path("/{id}/messages/attachments")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.APPLICATION_JSON)
    public Response findChatAttachments(@PathParam("id") long id) {
        List<AttachmentDto> attachments = attachmentService.findAttachmentByChatId(id);
        return Response.ok(attachments).build();
    }

    @GET
    @Path("/{chatId}/messages/{messageId}/attachments")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.APPLICATION_JSON)
    public Response findMessageAttachments(@PathParam("chatId") long chatId,
                                           @PathParam("messageId") long messageId) {
        MessageDto message = messageService.findById(messageId);
        ChatDto chat = message.getChat();
        if(chat.getId() != chatId){
            throw new NotFoundException(NO_MESSAGE_IN_CHAT);
        }
        AttachmentDto attachment = attachmentService.findAttachmentByMessageId(messageId);
        return Response.ok(attachment).build();
    }

    @GET
    @Path("/{chatId}/messages/{messageId}/attachments/{id}")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public Response getAttachment(@PathParam("chatId") long chatId,
                                  @PathParam("messageId") long messageId,
                                  @PathParam("id") String id) {
        AttachmentDto attachment = attachmentService.findAttachmentByMessageIdAndObjectId(messageId, id);
        MessageDto message = attachment.getMessage();
        ChatDto chat = message.getChat();
        if(chat.getId() != chatId){
            throw new NotFoundException(NO_MESSAGE_IN_CHAT);
        }
        ContentStream contentStream = attachmentService.getContent(id);
        return Response.ok(contentStream.getStream(), contentStream.getMimeType())
                .header("content-disposition", "attachment; filename = " + message.getText())
                .build();
    }

    private String defineAttachmentDocumentName(String fileName, long messageId){
        String fileExtension = attachmentService.defineFileExtension(fileName);
        return Long.toString(messageId).concat(".").concat(fileExtension);
    }
}

