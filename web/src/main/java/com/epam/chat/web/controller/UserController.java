package com.epam.chat.web.controller;

import com.epam.chat.model.dto.UserDto;
import com.epam.chat.model.dto.response.ChatDto;
import com.epam.chat.model.dto.response.MembershipDto;
import com.epam.chat.service.ChatService;
import com.epam.chat.service.MembershipService;
import com.epam.chat.service.UserService;
import com.epam.chat.web.security.filter.binding.Secured;
import com.sap.security.um.user.PersistenceException;
import com.sap.security.um.user.UnsupportedUserAttributeException;
import com.sap.security.um.user.User;
import com.sap.security.um.user.UserProvider;
import io.swagger.annotations.*;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/users")
@Api("Chat")
public class UserController {

    private static final String FORBIDDEN_ERROR_MESSAGE = "Given login doesn't match with your SAP Cloud id";
    private static final String USER_NAME_ATTRIBUTE = "name";

    private UserService userService;
    private MembershipService membershipService;
    private UserProvider userProvider;
    private ChatService chatService;

    @Inject
    public UserController(UserService userService, MembershipService membershipService, UserProvider userProvider, ChatService chatService) {
        this.userService = userService;
        this.membershipService = membershipService;
        this.userProvider = userProvider;
        this.chatService = chatService;
    }

    @ApiOperation(value = "Get current user", response = UserDto.class)
    @ApiResponse(code = 200, message = "Successfully get user")
    @GET
    @Path("/current")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCurrentUser() throws PersistenceException, UnsupportedUserAttributeException {
        User currentUser = userProvider.getCurrentUser();
        String login = currentUser.getAttribute(USER_NAME_ATTRIBUTE);
        UserDto userDto = new UserDto();
        userDto.setLogin(login);
        return Response
                .ok(userDto)
                .build();
    }

    @ApiOperation(value = "Get user by login", response = UserDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully find user"),
            @ApiResponse(code = 404, message = "User with given login not found")
    })
    @GET
    @Path("/{login}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getUserByLogin(@PathParam("login") String login) throws PersistenceException, UnsupportedUserAttributeException {
        User user = userProvider.getCurrentUser();
        String currentUserLogin = user.getAttribute(USER_NAME_ATTRIBUTE);

        if (!currentUserLogin.equals(login)) {
            throw new ForbiddenException(FORBIDDEN_ERROR_MESSAGE);
        }

        UserDto userDto = userService.findByLogin(login);
        return Response
                .ok(userDto)
                .build();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response findAllUsers() {
        List<UserDto> users = userService.findAllUsers();
        return Response
                .ok(users)
                .build();
    }

    @ApiOperation(value = "Add new user", response = UserDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Successfully added user"),
            @ApiResponse(code = 400, message = "Invalid user fields"),
            @ApiResponse(code = 400, message = "User with given login already exists")
    })
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addUser(@Valid UserDto userDto) throws PersistenceException, UnsupportedUserAttributeException {
        User user = userProvider.getCurrentUser();
        String login = user.getAttribute(USER_NAME_ATTRIBUTE);

        if (!login.equals(userDto.getLogin())) {
            throw new ForbiddenException(FORBIDDEN_ERROR_MESSAGE);
        }

        UserDto createdUser = userService.save(userDto);
        return Response.status(Response.Status.CREATED).entity(createdUser).build();
    }

    @ApiOperation(value = "Find user's memberships", response = List.class, authorizations = {
            @Authorization(value = "basicAuth")
    })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully found user's memberships")
    })
    @GET
    @Secured
    @Path("/{login}/memberships")
    @Produces(MediaType.APPLICATION_JSON)
    public Response findUsersMemberships(@PathParam("login") String login) throws PersistenceException, UnsupportedUserAttributeException {
        User user = userProvider.getCurrentUser();
        String userLogin = user.getAttribute(USER_NAME_ATTRIBUTE);
        if (!userLogin.equals(login)) {
            throw new ForbiddenException(FORBIDDEN_ERROR_MESSAGE);
        }
        List<MembershipDto> usersMemberships = membershipService.findByUserLogin(userLogin);
        return Response.ok(usersMemberships).build();
    }


    @ApiOperation(value = "Find user's chats", response = List.class, authorizations = {
            @Authorization(value = "basicAuth")
    })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully found user's chats")
    })
    @GET
    @Secured
    @Path("/{login}/chats")
    @Produces(MediaType.APPLICATION_JSON)
    public Response findUserChats(@PathParam("login") String login) throws PersistenceException, UnsupportedUserAttributeException {
        User user = userProvider.getCurrentUser();
        String userLogin = user.getAttribute(USER_NAME_ATTRIBUTE);
        if (!userLogin.equals(login)) {
            throw new ForbiddenException(FORBIDDEN_ERROR_MESSAGE);
        }
        List<ChatDto> usersMemberships = chatService.findChatsByUserLogin(userLogin);
        return Response.ok(usersMemberships).build();
    }

    @ApiOperation("Delete user")
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "User has been deleted successfully"),
            @ApiResponse(code = 403, message = "User can't delete other users"),
            @ApiResponse(code = 404, message = "User hasn't been found")
    })
    @DELETE
    @Path("/{login}")
    @Secured
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteAccount(@PathParam("login") String login) throws PersistenceException, UnsupportedUserAttributeException {
        User user = userProvider.getCurrentUser();
        String currentUserLogin = user.getAttribute(USER_NAME_ATTRIBUTE);
        if (!currentUserLogin.equals(login)) {
            throw new ForbiddenException(FORBIDDEN_ERROR_MESSAGE);
        }
        userService.delete(login);
        return Response.noContent().build();
    }

}
