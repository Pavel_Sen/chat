package com.epam.chat.web.mapper;

import com.epam.chat.model.dto.response.ErrorDto;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class GenericExceptionMapper implements ExceptionMapper<Exception> {

    @Override
    public Response toResponse(Exception exception) {
        Response.StatusType type = getStatusType(exception);

        ErrorDto errorDto = new ErrorDto();
        errorDto.setCode(type.getStatusCode());
        errorDto.setMessage(type.getReasonPhrase());
        errorDto.setDescription(exception.getLocalizedMessage());

        return Response.status(type.getStatusCode())
                .entity(errorDto)
                .type(MediaType.APPLICATION_JSON)
                .build();
    }

    private Response.StatusType getStatusType(Throwable ex) {
        if (ex instanceof WebApplicationException) {
            return ((WebApplicationException) ex).getResponse().getStatusInfo();
        } else {
            return Response.Status.INTERNAL_SERVER_ERROR;
        }
    }
}
