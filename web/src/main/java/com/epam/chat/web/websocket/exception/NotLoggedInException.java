package com.epam.chat.web.websocket.exception;

public class NotLoggedInException extends Exception {

    public NotLoggedInException(String message) {
        super(message);
    }
}
