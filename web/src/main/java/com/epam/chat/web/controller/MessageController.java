package com.epam.chat.web.controller;

import com.epam.chat.model.dto.MessageCreateOrUpdateDto;
import com.epam.chat.model.dto.response.ChatDto;
import com.epam.chat.model.dto.response.MessageDto;
import com.epam.chat.service.MembershipService;
import com.epam.chat.service.MessageService;
import com.epam.chat.web.security.filter.binding.Secured;
import com.sap.security.um.user.PersistenceException;
import com.sap.security.um.user.UnsupportedUserAttributeException;
import com.sap.security.um.user.User;
import com.sap.security.um.user.UserProvider;
import io.swagger.annotations.*;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/messages")
@Api(value = "Chat")
@Secured
public class MessageController {

    private static final String FORBIDDEN_ERROR = "Request forbidden";
    private static final String USER_NAME_ATTRIBUTE = "name";

    private MessageService messageService;
    private MembershipService membershipService;
    private UserProvider userProvider;

    @Inject
    public MessageController(MessageService messageService, MembershipService membershipService, UserProvider userProvider) {
        this.messageService = messageService;
        this.membershipService = membershipService;
        this.userProvider = userProvider;
    }

    @ApiOperation(value = "Get message by id", response = MessageDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully found message"),
            @ApiResponse(code = 404, message = "Message with given id not found"),
            @ApiResponse(code = 403, message = "Only chat member can see this message")
    })
    @GET
    @Path("/{id}")
    @Produces({MediaType.APPLICATION_JSON})
    public Response findMessageById(@ApiParam(value = "Message id to get object", required = true) @PathParam("id") long id) throws UnsupportedUserAttributeException, PersistenceException {
        User user = userProvider.getCurrentUser();
        String currentUserLogin = user.getAttribute(USER_NAME_ATTRIBUTE);
        MessageDto message = messageService.findById(id);
        if (!isThereUserAccess(message, currentUserLogin)) {
            throw new ForbiddenException(FORBIDDEN_ERROR);
        }
        return Response.ok(message).build();
    }

    @ApiOperation(value = "Get all user's messages", response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully found all user's messages"),
            @ApiResponse(code = 403, message = "User is not member of chat")
    })
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public Response findMessagesByCurrentUser(@QueryParam("limit") @Min(0) Long limit,
                                              @QueryParam("offset") @Min(0) @DefaultValue("0") Integer offset) throws PersistenceException, UnsupportedUserAttributeException {

        User user = userProvider.getCurrentUser();
        String currentUserLogin = user.getAttribute(USER_NAME_ATTRIBUTE);

        Long fieldsCount = messageService.getMessagesCountByUserLogin(currentUserLogin);
        if (limit == null) {
            limit = fieldsCount;
        }

        List<MessageDto> messages = messageService.findMessagesByUserLoginWithPagination(currentUserLogin, limit, offset);
        return Response
                .ok(messages)
                .header("X-Total-Count", limit)
                .build();
    }

    @ApiOperation("Delete message")
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "Successfully deleted message"),
            @ApiResponse(code = 404, message = "Message with given id doesn't exist")
    })
    @DELETE
    @Path("/{id}")
    @Produces({MediaType.APPLICATION_JSON})
    public Response deleteMessage(@ApiParam(value = "Message id to delete object", required = true) @PathParam("id") long id) throws PersistenceException, UnsupportedUserAttributeException {
        User user = userProvider.getCurrentUser();
        String currentUserLogin = user.getAttribute(USER_NAME_ATTRIBUTE);
        messageService.deleteById(id, currentUserLogin);
        return Response.noContent().build();
    }

    @ApiOperation(value = "Update message", response = MessageDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully updated message"),
            @ApiResponse(code = 404, message = "Message with given id is not found"),
            @ApiResponse(code = 400, message = "Invalid message fields values")
    })
    @PUT
    @Path("/{id}")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})

    public Response updateMessage(@PathParam("id") long id, @Valid MessageCreateOrUpdateDto message) throws PersistenceException, UnsupportedUserAttributeException {
        User user = userProvider.getCurrentUser();
        String currentUserLogin = user.getAttribute(USER_NAME_ATTRIBUTE);
        MessageDto updatedMessage = messageService.update(id, message, currentUserLogin);
        return Response.ok(updatedMessage).build();
    }

    private boolean isThereUserAccess(MessageDto messageDto, String currentUserLogin) {
        ChatDto chatDto = messageDto.getChat();
        long chatId = chatDto.getId();
        return membershipService.isUserChatMember(chatId, currentUserLogin);
    }
}
