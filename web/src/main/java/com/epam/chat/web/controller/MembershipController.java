package com.epam.chat.web.controller;

import com.epam.chat.model.dto.MembershipCreateDto;
import com.epam.chat.model.dto.UserDto;
import com.epam.chat.model.dto.response.MembershipDto;
import com.epam.chat.model.enums.UserRole;
import com.epam.chat.service.ChatService;
import com.epam.chat.service.MembershipService;
import com.epam.chat.service.MessageService;
import com.epam.chat.web.security.filter.binding.Secured;
import com.sap.security.um.user.PersistenceException;
import com.sap.security.um.user.UnsupportedUserAttributeException;
import com.sap.security.um.user.User;
import com.sap.security.um.user.UserProvider;
import io.swagger.annotations.*;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Api(value = "Chat")
@Path("/chats")
@Secured
public class MembershipController {

    private static final String FORBIDDEN_ERROR_MESSAGE = "Access denied";
    private static final String MEMBERSHIP_CREATED = "User %s has been added to chat";
    private static final String MEMBERSHIP_DELETED = "User %s has been removed from chat";
    private static final String USER_NAME_ATTRIBUTE = "name";

    private MembershipService membershipService;
    private MessageService messageService;
    private UserProvider userProvider;
    private ChatService chatService;

    @Inject
    public MembershipController(MembershipService membershipService, UserProvider userProvider,
                                MessageService messageService, ChatService chatService) {
        this.membershipService = membershipService;
        this.userProvider = userProvider;
        this.messageService = messageService;
        this.chatService = chatService;
    }

    @ApiOperation(value = "Find chat memberships", response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully found chat memberships"),
            @ApiResponse(code = 404, message = "Chat with such id doesn't exist"),
            @ApiResponse(code = 403, message = "Forbidden. Only chat members can see chat memberships")

    })
    @GET
    @Path("/{id}/memberships")
    @Produces({MediaType.APPLICATION_JSON})
    public Response findChatMemberships(@PathParam("id") Long id) throws PersistenceException, UnsupportedUserAttributeException {
        User user = userProvider.getCurrentUser();
        String currentUserLogin = user.getAttribute(USER_NAME_ATTRIBUTE);
        if (!membershipService.isUserChatMember(id, currentUserLogin)) {
            throw new ForbiddenException(FORBIDDEN_ERROR_MESSAGE);
        }
        List<MembershipDto> chatMemberships = membershipService.findByChatId(id);
        return Response.ok(chatMemberships).build();
    }

    @POST
    @ApiOperation(value = "Add new membership to chat", response = MembershipDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Successfully added membership"),
            @ApiResponse(code = 400, message = "Invalid chat fields"),
            @ApiResponse(code = 403, message = "Forbidden add membership"),
            @ApiResponse(code = 404, message = "User not found"),
            @ApiResponse(code = 404, message = "Chat not found")
    })
    @Path("/{chatId}/memberships")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response createMembership(@PathParam(value = "chatId") long chatId,
                                     @ApiParam(value = "Membership object to saveContent in database", required = true) @Valid MembershipCreateDto membershipCreateDto) throws PersistenceException, UnsupportedUserAttributeException {
        User user = userProvider.getCurrentUser();
        String currentUserLogin = user.getAttribute(USER_NAME_ATTRIBUTE);
        MembershipDto membershipDto = membershipService.save(membershipCreateDto, currentUserLogin, chatId);
        UserDto addedUser = membershipCreateDto.getUser();
        String addedUserLogin = addedUser.getLogin();
        messageService.saveEvent(chatId, String.format(MEMBERSHIP_CREATED, addedUserLogin));
        return Response.status(Response.Status.CREATED).entity(membershipDto).build();
    }

    @ApiOperation("Delete membership")
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "Successfully deleted membership"),
            @ApiResponse(code = 404, message = "Membership with given chat id and username doesn't exist")
    })
    @DELETE
    @Path("/{chatId}/users/{userToDelete}")
    @Produces({MediaType.APPLICATION_JSON})
    public Response deleteMembership(@PathParam("chatId") long chatId,
                                     @PathParam("userToDelete") String userToDelete) throws PersistenceException, UnsupportedUserAttributeException {
        User user = userProvider.getCurrentUser();
        String currentUserLogin = user.getAttribute(USER_NAME_ATTRIBUTE);
        if (!membershipService.isUserChatMember(chatId, currentUserLogin) && !membershipService.isUserChatMember(chatId, userToDelete)) {
            throw new ForbiddenException(FORBIDDEN_ERROR_MESSAGE);
        }
        MembershipDto membershipDto = membershipService.findByChatIdAndUserLogin(chatId, currentUserLogin);
        if (membershipDto.getUserRole().equals(UserRole.USER)) {
            if (!userToDelete.equals(currentUserLogin)) {
                throw new ForbiddenException(FORBIDDEN_ERROR_MESSAGE);
            }
        }
        membershipService.removeByChatIdAndUserLogin(chatId, userToDelete);
        if(chatService.isChatPresent(chatId)){
            messageService.saveEvent(chatId, String.format(MEMBERSHIP_DELETED, userToDelete));
        }
        return Response.noContent().build();
    }

    @GET
    @Path("/{id}/users/{userLogin}")
    @Produces({MediaType.APPLICATION_JSON})
    public Response findChatMemberships(@PathParam("id") Long id,
                                        @PathParam("userLogin") String userLogin) throws PersistenceException, UnsupportedUserAttributeException {
        User user = userProvider.getCurrentUser();
        String currentUserLogin = user.getAttribute(USER_NAME_ATTRIBUTE);
        if (!membershipService.isUserChatMember(id, currentUserLogin)) {
            throw new ForbiddenException(FORBIDDEN_ERROR_MESSAGE);
        }
        MembershipDto membership = membershipService.findByChatIdAndUserLogin(id, userLogin);
        return Response.ok(membership).build();
    }
}
