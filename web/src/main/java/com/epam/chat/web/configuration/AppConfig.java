package com.epam.chat.web.configuration;

import com.epam.chat.model.entity.*;
import com.epam.chat.repository.Repository;
import com.epam.chat.repository.supplier.*;
import com.epam.chat.service.*;
import com.epam.chat.service.impl.*;
import com.sap.security.um.service.UserManagementAccessor;
import com.sap.security.um.user.PersistenceException;
import com.sap.security.um.user.UserProvider;
import io.swagger.jaxrs.config.BeanConfig;
import io.swagger.jaxrs.config.SwaggerContextService;
import io.swagger.jaxrs.listing.ApiListingResource;
import io.swagger.models.Swagger;
import io.swagger.v3.jaxrs2.SwaggerSerializers;
import org.glassfish.hk2.api.TypeLiteral;
import org.glassfish.jersey.internal.inject.AbstractBinder;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.process.internal.RequestScoped;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.ServerProperties;
import org.glassfish.jersey.server.filter.RolesAllowedDynamicFeature;

import javax.inject.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.InternalServerErrorException;
import java.lang.reflect.Type;

@ApplicationPath("api/v1")
public class AppConfig extends ResourceConfig {

    public AppConfig() {

        packages(true, "com.epam.chat");
        register(JacksonFeature.class);
        register(MultiPartFeature.class);
        property(ServerProperties.BV_SEND_ERROR_IN_RESPONSE, true);
        property(ServerProperties.BV_DISABLE_VALIDATE_ON_EXECUTABLE_OVERRIDE_CHECK, true);
        register(new AbstractBinder() {
            @Override
            protected void configure() {
                bindFactory(EntityManagerFactorySupplier.class).to(EntityManagerFactory.class).in(Singleton.class);
                bindFactory(EntityManagerSupplier.class).to(EntityManager.class).in(RequestScoped.class);

                Type messageRepositoryType = new TypeLiteral<Repository<Message>>() {
                }.getType();
                bindFactory(MessageRepositorySupplier.class).to(messageRepositoryType).in(RequestScoped.class);

                Type userRepositoryType = new TypeLiteral<Repository<User>>() {
                }.getType();
                bindFactory(UserRepositorySupplier.class).to(userRepositoryType).in(RequestScoped.class);

                Type chatRepositoryType = new TypeLiteral<Repository<Chat>>() {
                }.getType();
                bindFactory(ChatRepositorySupplier.class).to(chatRepositoryType).in(RequestScoped.class);

                Type membershipRepositoryType = new TypeLiteral<Repository<Membership>>() {
                }.getType();
                bindFactory(MembershipRepositorySupplier.class).to(membershipRepositoryType).in(RequestScoped.class);

                Type attachmentRepositoryType = new TypeLiteral<Repository<Attachment>>() {
                }.getType();
                bindFactory(AttachmentRepositorySupplier.class).to(attachmentRepositoryType).in(RequestScoped.class);

                bind(MembershipServiceImpl.class).to(MembershipService.class).in(RequestScoped.class);
                bind(MessageServiceImpl.class).to(MessageService.class).in(RequestScoped.class);
                bind(ChatServiceImpl.class).to(ChatService.class).in(RequestScoped.class);
                bind(UserServiceImpl.class).to(UserService.class).in(RequestScoped.class);
                bind(AttachmentServiceImpl.class).to(AttachmentService.class).in(RequestScoped.class);
                bind(AuthServiceImpl.class).proxy(true).proxyForSameScope(false).to(AuthService.class).in(RequestScoped.class);

                try {
                    UserProvider userProvider = UserManagementAccessor.getUserProvider();
                    bind(userProvider).to(UserProvider.class).to(Singleton.class);
                } catch (PersistenceException e) {
                    throw new InternalServerErrorException(e.getLocalizedMessage());
                }
            }
        });

        configureSwagger();
    }

    private void configureSwagger() {
        BeanConfig beanConfig = new BeanConfig();
        beanConfig.setTitle("Swagger");
        beanConfig.setVersion("1.0.0");
        beanConfig.setSchemes(new String[]{"https", "http"});
        beanConfig.setHost("chatp2001476468trial.hanatrial.ondemand.com");
        beanConfig.setBasePath("chat/api/v1");
        beanConfig.setScan(true);
        beanConfig.setResourcePackage(
                "com.epam.chat.web");
        packages("io.swagger.jaxrs.listing");

        Swagger swagger = new Swagger();
        new SwaggerContextService().updateSwagger(swagger);
        this.register(RolesAllowedDynamicFeature.class);
        this.register(ApiListingResource.class);
        this.register(SwaggerSerializers.class);
    }
}
