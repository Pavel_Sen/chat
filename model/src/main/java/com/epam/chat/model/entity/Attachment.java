package com.epam.chat.model.entity;

import com.epam.chat.model.enums.AttachmentType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;

@Data
@Entity
@Table
public class Attachment implements BaseEntity {

    @Id
    private String id;

    @OneToOne
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private Message message;

    @Enumerated(EnumType.STRING)
    private AttachmentType type;
}
