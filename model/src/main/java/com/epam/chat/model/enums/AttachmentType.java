package com.epam.chat.model.enums;

public enum  AttachmentType {
    IMAGE, AUDIO, VIDEO, FILE
}
