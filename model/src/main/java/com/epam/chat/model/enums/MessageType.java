package com.epam.chat.model.enums;

public enum MessageType {
    MESSAGE, EVENT, ATTACHMENT
}
