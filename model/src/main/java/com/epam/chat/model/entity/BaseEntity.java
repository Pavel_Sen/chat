package com.epam.chat.model.entity;

import javax.persistence.MappedSuperclass;

@MappedSuperclass
public interface BaseEntity {}
