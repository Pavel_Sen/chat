package com.epam.chat.model.dto.response;

import com.epam.chat.model.enums.ChatMode;
import com.epam.chat.model.enums.ChatType;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@Data
public class ChatDto {

    private Long id;

    private String name;

    private ChatType type;

    private ChatMode mode;

    @JsonIgnoreProperties("chat")
    private MessageDto lastMessage;
}
