package com.epam.chat.model.enums;

public enum ChatMode {
    NORMAL, BROADCAST
}
