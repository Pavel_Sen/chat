package com.epam.chat.model.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class UserDto {

    @NotNull(message = "Login required")
    @Size(min = 1, max = 30, message = "Login must be between 1 and 30")
    private String login;
}
