package com.epam.chat.model.dto.response;

import com.epam.chat.model.enums.AttachmentType;
import lombok.Data;

@Data
public class AttachmentDto {

    private String id;

    private MessageDto message;

    private AttachmentType type;

}
