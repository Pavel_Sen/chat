package com.epam.chat.model.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class ChatDtoEmbedded {

    @NotNull(message = "Id required")
    private Long id;

}
