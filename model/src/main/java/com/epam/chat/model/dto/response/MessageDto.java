package com.epam.chat.model.dto.response;

import com.epam.chat.model.dto.UserDto;
import com.epam.chat.model.enums.MessageType;
import com.epam.chat.model.util.LocalDateTimeSerializer;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class MessageDto {

    private Long id;

    @JsonSerialize(using = LocalDateTimeSerializer.class)
    private LocalDateTime creation;

    private String text;

    private UserDto user;

    @JsonIgnoreProperties("lastMessage")
    private ChatDto chat;

    private MessageType type;

}
