package com.epam.chat.service.impl;

import com.epam.chat.model.dto.ChatCreateOrUpdateDto;
import com.epam.chat.model.entity.Chat;
import com.epam.chat.model.entity.Membership;
import com.epam.chat.model.entity.Message;
import com.epam.chat.model.entity.User;
import com.epam.chat.repository.Repository;
import com.epam.chat.repository.impl.RepositoryImpl;
import com.epam.chat.service.ChatService;
import org.junit.Test;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.ws.rs.NotFoundException;

public class ChatServiceImplTest {

    private EntityManager entityManager;

    private ChatService chatService;

    private ChatCreateOrUpdateDto testChatDto;

    {
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("test");
        entityManager = entityManagerFactory.createEntityManager();
        Repository<Chat> chatRepository = new RepositoryImpl<>(entityManager, Chat.class);
        Repository<Membership> membershipRepository = new RepositoryImpl<>(entityManager, Membership.class);
        Repository<Message> messageRepository = new RepositoryImpl<>(entityManager, Message.class);
        Repository<User> userRepository = new RepositoryImpl<>(entityManager, User.class);
        chatService = new ChatServiceImpl(chatRepository, membershipRepository, messageRepository, userRepository);

        testChatDto = new ChatCreateOrUpdateDto();
        testChatDto.setName("testChat");
    }



    @Test(expected = NotFoundException.class)
    public void shouldThrowNotFoundExceptionWhenRemoveChatWithNotExistingId() {
        chatService.removeById(1L);
    }
}
