package com.epam.chat.service.impl;

import com.epam.chat.model.dto.ChatDtoEmbedded;
import com.epam.chat.model.dto.MessageCreateOrUpdateDto;
import com.epam.chat.model.dto.UserDto;
import com.epam.chat.model.dto.response.MessageDto;
import com.epam.chat.model.entity.Chat;
import com.epam.chat.model.entity.Membership;
import com.epam.chat.model.entity.Message;
import com.epam.chat.model.entity.User;
import com.epam.chat.model.enums.UserRole;
import com.epam.chat.repository.Repository;
import com.epam.chat.repository.specification.impl.message.FindAllMessageSpecification;
import com.epam.chat.repository.specification.impl.message.FindMessageByIdSpecification;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import javax.ws.rs.NotFoundException;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;


@RunWith(MockitoJUnitRunner.class)
public class MessageServiceImplTest {

    @Mock
    private Repository<Message> messageRepository;

    @InjectMocks
    private MessageServiceImpl messageService;

    private Long messageId = 1L;
    private String userLogin = "login";
    private String messageText = "message text";
    private LocalDateTime dateCreation = LocalDateTime.now();
    private Message message;
    private MessageCreateOrUpdateDto messageCreateOrUpdateDto;

    @Before
    public void initData() {
        MockitoAnnotations.initMocks(this);
        message = new Message();
        User user = new User();
        user.setLogin(userLogin);
        message.setUser(user);
        Chat chat = new Chat();
        chat.setId(1L);
        message.setChat(chat);
        message.setText(messageText);
        message.setCreation(dateCreation);


        UserDto userDto = new UserDto();
        userDto.setLogin(userLogin);

        messageCreateOrUpdateDto = new MessageCreateOrUpdateDto();
        messageCreateOrUpdateDto.setText(messageText);

        ChatDtoEmbedded chatDto = new ChatDtoEmbedded();
        chatDto.setId(1L);

        Membership membership = new Membership();
        membership.setUser(user);
        membership.setChat(chat);
        membership.setUserRole(UserRole.ADMIN);
    }

    @Test
    public void shouldReturnSpecificMessageByItsId() {
        message.setId(messageId);
        when(messageRepository.findSingleBy(any(FindMessageByIdSpecification.class))).thenReturn(Optional.of(message));

        MessageDto returnedMessageDto = messageService.findById(messageId);
        Assert.assertEquals(messageId, returnedMessageDto.getId());
        Assert.assertEquals(messageText, returnedMessageDto.getText());
        Assert.assertEquals(userLogin, returnedMessageDto.getUser().getLogin());
    }

    @Test
    public void shouldReturnListOfAllMessages() {
        message.setId(messageId);
        when(messageRepository.findBy(any(FindAllMessageSpecification.class))).thenReturn(Arrays.asList(message));

        List<MessageDto> messages = messageService.findAll();
        Assert.assertEquals(1, messages.size());

        MessageDto returnedDto = messages.get(0);

        Assert.assertEquals(messageId, returnedDto.getId());
        Assert.assertEquals(messageText, returnedDto.getText());
        Assert.assertEquals(dateCreation, returnedDto.getCreation());
        Assert.assertEquals(userLogin, returnedDto.getUser().getLogin());
    }

    @Test
    public void shouldReturnUpdatedMessageWhenInvokeUpdateOfMessageServiceImpl() {
        message.setId(1L);
        when(messageRepository.findSingleBy(any(FindMessageByIdSpecification.class))).thenReturn(Optional.of(message));
        when(messageRepository.save(any(Message.class))).thenReturn(message);

        MessageDto updatedMessage = messageService.update(1L, messageCreateOrUpdateDto, userLogin);

        Assert.assertEquals(messageId, updatedMessage.getId());
        Assert.assertEquals(messageText, updatedMessage.getText());
        Assert.assertEquals(dateCreation, updatedMessage.getCreation());
        Assert.assertEquals(userLogin, updatedMessage.getUser().getLogin());
    }

    @Test
    public void repositoryShouldInvokeDeletingWhenInvokeDeleteOfMessageServiceImpl() {
        when(messageRepository.findSingleBy(any(FindMessageByIdSpecification.class))).thenReturn(Optional.of(message));

        messageService.deleteById(messageId, userLogin);

        verify(messageRepository, times(1)).removeById(messageId);
    }

    @Test(expected = NotFoundException.class)
    public void shouldThrowNotFoundExceptionWhenCantFindMessageById() throws NotFoundException {
        long missingId = 1L;
        when(messageRepository.findSingleBy(any(FindMessageByIdSpecification.class))).thenReturn(Optional.empty());
        messageService.findById(missingId);
    }

    @Test(expected = NotFoundException.class)
    public void shouldThrowNotFoundExceptionWhenUpdateMissingMessage() throws NotFoundException {
        long missingId = 1L;
        when(messageRepository.findSingleBy(any(FindMessageByIdSpecification.class))).thenReturn(Optional.empty());
        messageService.update(missingId, messageCreateOrUpdateDto, userLogin);
    }

    @Test(expected = NotFoundException.class)
    public void shouldThrowNotFoundExceptionWhenDeleteMissingMessage() throws NotFoundException {
        long missingId = 1L;
        when(messageRepository.findSingleBy(any(FindMessageByIdSpecification.class))).thenReturn(Optional.empty());
        messageService.deleteById(missingId, userLogin);
    }
}
