package com.epam.chat.service.impl;

import com.epam.chat.model.dto.response.AttachmentDto;
import com.epam.chat.model.dto.response.MessageDto;
import com.epam.chat.model.entity.Attachment;
import com.epam.chat.model.entity.Message;
import com.epam.chat.model.enums.AttachmentType;
import com.epam.chat.repository.specification.impl.attachment.AttachmentByChatIdSpecification;
import com.epam.chat.repository.specification.impl.attachment.AttachmentByMessageIdAndObjectIdSpecification;
import com.epam.chat.repository.specification.impl.attachment.AttachmentByMessageIdSpecification;
import com.epam.chat.repository.specification.impl.message.FindMessageByIdSpecification;
import com.epam.chat.service.AttachmentService;
import com.epam.chat.service.helper.CMISSessionHelper;
import com.epam.chat.repository.Repository;

import org.apache.chemistry.opencmis.client.api.Document;
import org.apache.chemistry.opencmis.client.api.Folder;
import org.apache.chemistry.opencmis.client.api.Session;
import org.apache.chemistry.opencmis.commons.PropertyIds;
import org.apache.chemistry.opencmis.commons.data.ContentStream;
import org.apache.chemistry.opencmis.commons.enums.VersioningState;
import org.apache.chemistry.opencmis.commons.exceptions.CmisNameConstraintViolationException;
import org.modelmapper.ModelMapper;

import javax.inject.Inject;
import javax.persistence.EntityTransaction;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.NotFoundException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

public class AttachmentServiceImpl implements AttachmentService {

    private static final int BUFFER_LENGTH = 1024;
    private static final long MAX_FILE_SIZE = 5 * BUFFER_LENGTH * BUFFER_LENGTH;

    private static final String FILE_UPLOAD_ERROR = "Impossible to save Content file";
    private static final String INVALID_FILE_SIZE = "Max file size is 5 MB";
    private static final String OBJECT_TYPE_ID = "cmis:document";
    private static final String MIME_TYPE = "%s; charset=UTF-8";

    private static final String MESSAGE_NOT_FOUND = "Message is not found : ";
    private static final String ATTACHMENT_NOT_FOUND = "Attachment not found: ";
    private static final String ATTACHMENT_FOR_MESSAGE_NOT_FOUND = "There is no attachment for message with id: ";
    private static final String ATTACHMENT_EXISTS = "Attachment for this message already exists";

    private ModelMapper modelMapper = new ModelMapper();

    private Repository<Attachment> attachmentRepository;
    private Repository<Message> messageRepository;

    @Inject
    public AttachmentServiceImpl(Repository<Attachment> attachmentRepository, Repository<Message> messageRepository){
        this.attachmentRepository = attachmentRepository;
        this.messageRepository = messageRepository;
    }

    @Override
    public String saveContent(String mediaType, String documentName, InputStream stream) {

        String objectId;
        try {
            long fileSize = stream.available();
            if(fileSize > MAX_FILE_SIZE){
                throw new BadRequestException(INVALID_FILE_SIZE);
            }
            Session session = CMISSessionHelper.getInstance().getSession();

            Folder root = session.getRootFolder();

            Map<String, Object> properties = new HashMap<>();
            properties.put(PropertyIds.OBJECT_TYPE_ID, OBJECT_TYPE_ID);
            properties.put(PropertyIds.NAME, documentName);

            ContentStream contentStream = session.getObjectFactory()
                    .createContentStream(documentName, fileSize, String.format(MIME_TYPE, mediaType), stream);
            try {
                Document document = root.createDocument(properties, contentStream, VersioningState.NONE);
                objectId = document.getId();
            } catch (CmisNameConstraintViolationException e) {
                throw new BadRequestException(FILE_UPLOAD_ERROR);
            }
        } catch (Exception e) {
            throw new BadRequestException(FILE_UPLOAD_ERROR);
        }
        return objectId;
    }

    @Override
    public AttachmentDto save(String id, MessageDto message) {
        long messageId = message.getId();
        messageRepository.findSingleBy(new FindMessageByIdSpecification(messageId))
                .orElseThrow(() -> new NotFoundException(MESSAGE_NOT_FOUND + messageId));

        Optional<Attachment> attachmentOptional = attachmentRepository.findSingleBy(new AttachmentByMessageIdSpecification(messageId));
        if(attachmentOptional.isPresent()){
            throw new BadRequestException(ATTACHMENT_EXISTS);
        }
        Attachment attachmentToSave = new Attachment();
        attachmentToSave.setId(id);
        attachmentToSave.setMessage(modelMapper.map(message, Message.class));
        attachmentToSave.setType(defineAttachmentType(message.getText()));

        attachmentRepository.transactionManage(EntityTransaction::begin);
        Attachment attachment = attachmentRepository.save(attachmentToSave);
        attachmentRepository.transactionManage(EntityTransaction::commit);

        return modelMapper.map(attachment, AttachmentDto.class);
    }

    @Override
    public String defineFileExtension(String fileName){
        return fileName.substring(fileName.lastIndexOf(".") + 1);
    }

    @Override
    public AttachmentDto findAttachmentByMessageId(long messageId) {
        Attachment attachment = attachmentRepository.findSingleBy(new AttachmentByMessageIdSpecification(messageId))
                .orElseThrow(() -> new NotFoundException(ATTACHMENT_FOR_MESSAGE_NOT_FOUND +  messageId));
        return modelMapper.map(attachment, AttachmentDto.class);
    }

    @Override
    public List<AttachmentDto> findAttachmentByChatId(Long chatId) {
        List<Attachment> attachments = attachmentRepository.findBy(new AttachmentByChatIdSpecification(chatId));
        return attachments.stream().map(attachment -> modelMapper.map(attachment, AttachmentDto.class)).collect(Collectors.toList());
    }

    @Override
    public ContentStream getContent(String objectId) {
        Session session = CMISSessionHelper.getInstance().getSession();
        Document doc = (Document) session.getObject(objectId);
        ContentStream contentStream = doc.getContentStream();
        if(contentStream == null){
            throw new NotFoundException(ATTACHMENT_NOT_FOUND + objectId);
        }
        return contentStream;
    }

    @Override
    public AttachmentDto findAttachmentByMessageIdAndObjectId(long messageId, String objectId) {
        Attachment attachment = attachmentRepository.findSingleBy(new AttachmentByMessageIdAndObjectIdSpecification(messageId, objectId))
                .orElseThrow(() -> new NotFoundException(ATTACHMENT_NOT_FOUND));
        return modelMapper.map(attachment, AttachmentDto.class);
    }

    private AttachmentType defineAttachmentType(String fileName){
        AttachmentType attachmentType;
        String fileExtension = defineFileExtension(fileName);
        switch (fileExtension){
            case "png":
            case "jpg":
            case "jpeg":
                attachmentType = AttachmentType.IMAGE;
                break;
            case "mp3":
                attachmentType = AttachmentType.AUDIO;
                break;
            case "mp4":
                attachmentType = AttachmentType.VIDEO;
                break;
            default:
                attachmentType = AttachmentType.FILE;
                break;
        }
        return attachmentType;
    }
}
