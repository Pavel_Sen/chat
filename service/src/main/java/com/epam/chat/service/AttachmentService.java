package com.epam.chat.service;

import com.epam.chat.model.dto.response.AttachmentDto;
import com.epam.chat.model.dto.response.MessageDto;
import org.apache.chemistry.opencmis.commons.data.ContentStream;

import java.io.InputStream;
import java.util.List;

public interface AttachmentService {
    String saveContent(String mediaType, String fileName, InputStream stream);

    AttachmentDto save(String id, MessageDto message);

    String defineFileExtension(String fileName);

    AttachmentDto findAttachmentByMessageId(long messageId);

    AttachmentDto findAttachmentByMessageIdAndObjectId(long messageId, String objectId);

    List<AttachmentDto> findAttachmentByChatId(Long chatId);

    ContentStream getContent(String objectId);
}
