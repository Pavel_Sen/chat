package com.epam.chat.service.impl;

import com.epam.chat.model.dto.ChatCreateOrUpdateDto;
import com.epam.chat.model.dto.response.ChatDto;
import com.epam.chat.model.dto.response.MessageDto;
import com.epam.chat.model.entity.Chat;
import com.epam.chat.model.entity.Membership;
import com.epam.chat.model.entity.Message;
import com.epam.chat.model.enums.ChatMode;
import com.epam.chat.model.enums.ChatType;
import com.epam.chat.repository.Repository;
import com.epam.chat.repository.specification.Specification;
import com.epam.chat.repository.specification.impl.chat.FindAllChatSpecification;
import com.epam.chat.repository.specification.impl.chat.FindChatByIdSpecification;
import com.epam.chat.repository.specification.impl.membership.FindMembershipByChatIdSpecification;
import com.epam.chat.repository.specification.impl.membership.FindMembershipByUserLoginAndChatId;
import com.epam.chat.repository.specification.impl.membership.FindMembershipByUserLoginSpecification;
import com.epam.chat.repository.specification.impl.message.FindMessageByChatIdSpecification;
import com.epam.chat.model.entity.User;
import com.epam.chat.model.enums.UserRole;
import com.epam.chat.repository.specification.impl.user.FindUserByLoginSpecification;
import com.epam.chat.service.ChatService;
import org.modelmapper.ModelMapper;

import javax.inject.Inject;
import javax.persistence.EntityTransaction;
import javax.ws.rs.NotFoundException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class ChatServiceImpl implements ChatService {

    private static final String CHAT_NOT_FOUND = "Chat is not found : ";
    private static final String USER_NOT_FOUND = "User is not found : ";

    private ModelMapper modelMapper = new ModelMapper();

    private Repository<Chat> chatRepository;
    private Repository<Membership> membershipRepository;
    private Repository<Message> messageRepository;
    private Repository<User> userRepository;

    @Inject
    public ChatServiceImpl(Repository<Chat> chatRepository, Repository<Membership> membershipRepository,
                           Repository<Message> messageRepository, Repository<User> userRepository) {

        this.chatRepository = chatRepository;
        this.membershipRepository = membershipRepository;
        this.messageRepository = messageRepository;
        this.userRepository = userRepository;
    }

    @Override
    public List<ChatDto> findAll() {
        List<Chat> chats = chatRepository.findBy(new FindAllChatSpecification());

        List<ChatDto> chatDtos = chats
                .stream()
                .map(chat -> modelMapper.map(chat, ChatDto.class))
                .collect(Collectors.toList());

        chatDtos.forEach(this::setLastMessage);
        return chatDtos;
    }

    @Override
    public ChatDto findById(Long id) {
        Chat chat = chatRepository.findSingleBy(new FindChatByIdSpecification(id))
                .orElseThrow(() -> new NotFoundException(CHAT_NOT_FOUND + id));

        ChatDto chatDto = modelMapper.map(chat, ChatDto.class);
        setLastMessage(chatDto);
        return chatDto;
    }

    @Override
    public ChatDto save(ChatCreateOrUpdateDto chatDto, String login) {
        User user = userRepository.findSingleBy(new FindUserByLoginSpecification(login))
                .orElseThrow(() -> new NotFoundException(USER_NOT_FOUND + login));
        Membership membership = new Membership();
        membership.setUser(user);
        membership.setUserRole(UserRole.ADMIN);
        chatRepository.transactionManage(EntityTransaction::begin);
        Chat chat = chatRepository.save(modelMapper.map(chatDto, Chat.class));
        membership.setChat(chat);
        membershipRepository.save(membership);
        chatRepository.transactionManage(EntityTransaction::commit);

        return modelMapper.map(chat, ChatDto.class);
    }

    @Override
    public ChatDto update(Long id, ChatCreateOrUpdateDto chatDto) {
        Chat chatToUpdate = chatRepository.findSingleBy(new FindChatByIdSpecification(id))
                .orElseThrow(() -> new NotFoundException(CHAT_NOT_FOUND + id));
        chatToUpdate.setName(chatDto.getName());
        chatToUpdate.setType(ChatType.valueOf(chatDto.getChatType()));
        chatToUpdate.setMode(ChatMode.valueOf(chatDto.getChatMode()));
        chatRepository.transactionManage(EntityTransaction::begin);
        Chat chat = chatRepository.save(chatToUpdate);
        chatRepository.transactionManage(EntityTransaction::commit);

        return modelMapper.map(chat, ChatDto.class);
    }

    @Override
    public void removeById(Long id) {
        chatRepository.findSingleBy(new FindChatByIdSpecification(id))
                .orElseThrow(() -> new NotFoundException(CHAT_NOT_FOUND + id));

        List<Membership> memberships = membershipRepository.findBy(new FindMembershipByChatIdSpecification(id));
        List<Message> messages = messageRepository.findBy(new FindMessageByChatIdSpecification(id));

        chatRepository.transactionManage(EntityTransaction::begin);
        messageRepository.remove(messages);
        membershipRepository.remove(memberships);
        chatRepository.removeById(id);
        chatRepository.transactionManage(EntityTransaction::commit);
    }

    @Override
    public boolean isUserChatAdmin(long chatId, String userLogin) {
        boolean result = false;
        Optional<Membership> membership = membershipRepository.findSingleBy(new FindMembershipByUserLoginAndChatId(userLogin, chatId));
        if (membership.isPresent()) {
            UserRole currentUserRole = membership.get().getUserRole();
            result = currentUserRole.equals(UserRole.ADMIN);
        }
        return result;
    }

    @Override
    public boolean isChatPresent(Long id) {
        return chatRepository.findSingleBy(new FindChatByIdSpecification(id)).isPresent();
    }

    @Override
    public List<ChatDto> findChatsByUserLogin(String userLogin) {
        List<Membership> memberships = membershipRepository.findBy(new FindMembershipByUserLoginSpecification(userLogin));
        List<ChatDto> chatDtos = memberships
                .stream()
                .map(membership -> modelMapper.map(membership.getChat(), ChatDto.class))
                .collect(Collectors.toList());
        chatDtos.forEach(this::setLastMessage);
        return chatDtos;
    }

    private ChatDto setLastMessage(ChatDto chatDto) {
        List<Message> messages = messageRepository.findBy(new FindMessageByChatIdSpecification(chatDto.getId()));
        MessageDto messageDto = new MessageDto();
        if (!messages.isEmpty()) {
            messageDto = modelMapper.map(messages.get(messages.size() - 1), MessageDto.class);
        }
        chatDto.setLastMessage(messageDto);
        return chatDto;
    }
}
