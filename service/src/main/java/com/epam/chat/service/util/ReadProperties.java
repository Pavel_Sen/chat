package com.epam.chat.service.util;

import javax.ws.rs.InternalServerErrorException;
import java.io.IOException;
import java.util.Properties;

public class ReadProperties {

    private static final String ERROR_LOADING_PROPERTIES = "Impossible to read properties from configuration file";

    private static ReadProperties instance = null;

    private Properties props;

    private ReadProperties(){
        props = new Properties();
        try {
            props.load(getClass().getResourceAsStream("/config.properties"));
        } catch (IOException e) {
            throw new InternalServerErrorException(ERROR_LOADING_PROPERTIES);
        }
    }

    public static synchronized ReadProperties getInstance(){
        if (instance == null)
            instance = new ReadProperties();
        return instance;
    }

    public String getValue(String propKey){
        return this.props.getProperty(propKey);
    }

}
