package com.epam.chat.service.exception;

public class CMISConnectionException  extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public CMISConnectionException(String errorMessage) {
        super(errorMessage);
    }

    public CMISConnectionException(Throwable throwable) {
        super(throwable);
    }

}
