package com.epam.chat.service.impl;

import com.epam.chat.model.dto.MembershipCreateDto;
import com.epam.chat.model.dto.UserDto;
import com.epam.chat.model.dto.response.MembershipDto;
import com.epam.chat.model.entity.Chat;
import com.epam.chat.model.entity.Membership;
import com.epam.chat.model.entity.Message;
import com.epam.chat.model.entity.User;
import com.epam.chat.model.enums.ChatType;
import com.epam.chat.model.enums.UserRole;
import com.epam.chat.repository.Repository;
import com.epam.chat.repository.specification.impl.chat.FindChatByIdSpecification;
import com.epam.chat.repository.specification.impl.membership.FindMembershipByChatIdSpecification;
import com.epam.chat.repository.specification.impl.membership.FindMembershipByUserLoginAndChatId;
import com.epam.chat.repository.specification.impl.membership.FindMembershipByUserLoginSpecification;
import com.epam.chat.repository.specification.impl.message.FindMessageByChatIdSpecification;
import com.epam.chat.repository.specification.impl.user.FindUserByLoginSpecification;
import com.epam.chat.service.MembershipService;
import org.modelmapper.ModelMapper;

import javax.inject.Inject;
import javax.persistence.EntityTransaction;
import javax.ws.rs.ForbiddenException;
import javax.ws.rs.NotFoundException;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class MembershipServiceImpl implements MembershipService {

    private static final String MEMBERSHIP_NOT_FOUND = "Membership is not found";
    private static final String USER_NOT_FOUND = "User login not found : ";
    private static final String CHAT_NOT_FOUND = "Chat id not found : ";
    private static final String ALREADY_REGISTERED = "Already registered in chat";
    private static final String CANT_ADD_ANYONE_TO_GROUP_CHAT = "User can't add anyone to group chat";
    private static final String USER_CANT_ADD_ADMIN = "User can't add Admin to chat";
    private static final String USER_NOT_IN_CHAT = "User is not in chat";
    private static final String CANT_REGISTER_IN_GROUP_CHAT = "Can't register in group chat";
    private static final String CANT_REGISTER_AS_ADMIN = "Can't register as Admin";

    private ModelMapper modelMapper = new ModelMapper();

    private Repository<Membership> membershipRepository;
    private Repository<Chat> chatRepository;
    private Repository<User> userRepository;
    @Inject Repository<Message> messageRepository;

    @Inject
    public MembershipServiceImpl(Repository<Membership> membershipRepository, Repository<Chat> chatRepository,
                                 Repository<User> userRepository) {
        this.membershipRepository = membershipRepository;
        this.chatRepository = chatRepository;
        this.userRepository = userRepository;
    }

    @Override
    public List<MembershipDto> findByChatId(Long chatId) {
        List<Membership> memberships = membershipRepository.findBy(new FindMembershipByChatIdSpecification(chatId));
        return memberships
                .stream()
                .map(membership -> modelMapper.map(membership, MembershipDto.class))
                .collect(Collectors.toList());
    }

    @Override
    public List<MembershipDto> findByUserLogin(String login) {
        List<Membership> memberships = membershipRepository.findBy(new FindMembershipByUserLoginSpecification(login));
        return memberships
                .stream()
                .map(membership -> modelMapper.map(membership, MembershipDto.class))
                .collect(Collectors.toList());
    }

    @Override
    public MembershipDto findByChatIdAndUserLogin(Long chatId, String login) {
        Membership membership = membershipRepository.findSingleBy(new FindMembershipByUserLoginAndChatId(login, chatId))
                .orElseThrow(() -> new NotFoundException(MEMBERSHIP_NOT_FOUND));

        return modelMapper.map(membership, MembershipDto.class);
    }

    @Override
    public MembershipDto save(MembershipCreateDto membershipCreateDto, String currentUserLogin, Long chatId) {
        UserDto userDto = membershipCreateDto.getUser();
        String userDtoLogin = userDto.getLogin();
        Chat chat = chatRepository.findSingleBy(new FindChatByIdSpecification(chatId))
                .orElseThrow(() -> new NotFoundException(CHAT_NOT_FOUND + chatId));

        if (currentUserLogin.equals(userDtoLogin)) {
           return registerToChat(membershipCreateDto, chat);

        }

        return addUserToChat(membershipCreateDto, currentUserLogin, chat);
    }

    @Override
    public void removeByChatIdAndUserLogin(Long chatId, String login) {
        Membership currentMembership = membershipRepository.findSingleBy(new FindMembershipByUserLoginAndChatId(login, chatId))
                .orElseThrow(() -> new NotFoundException(MEMBERSHIP_NOT_FOUND));

        List<Membership> memberships = membershipRepository.findBy(new FindMembershipByChatIdSpecification(chatId));
        memberships.remove(currentMembership);
        List<Membership> admins = memberships.stream().filter(membership -> UserRole.ADMIN.equals(membership.getUserRole())).collect(Collectors.toList());
        membershipRepository.transactionManage(EntityTransaction::begin);
        if (memberships.size() > 0) {
            if (admins.isEmpty()) {
                Random random = new Random();
                Membership membership = memberships.get(random.nextInt(memberships.size() - 1));
                membership.setUserRole(UserRole.ADMIN);
                membershipRepository.save(membership);
            }
        } else {
            List<Message> messages = messageRepository.findBy(new FindMessageByChatIdSpecification(chatId));
            messageRepository.remove(messages);
            chatRepository.removeById(chatId);
        }
        membershipRepository.remove(currentMembership);
        membershipRepository.transactionManage(EntityTransaction::commit);
    }

    @Override
    public boolean isUserChatMember(Long chatId, String login) {
        return membershipRepository.findSingleBy(new FindMembershipByUserLoginAndChatId(login, chatId)).isPresent();
    }

    private MembershipDto addUserToChat(MembershipCreateDto membershipCreateDto, String currentUser, Chat chat) {
        UserDto userDto = membershipCreateDto.getUser();
        String loginUserToAdd = userDto.getLogin();
        Long chatId = chat.getId();
        Membership membership = membershipRepository.findSingleBy(new FindMembershipByUserLoginAndChatId(currentUser, chatId))
                .orElseThrow(() -> new ForbiddenException(USER_NOT_IN_CHAT));
        if (isUserChatMember(chatId, loginUserToAdd)) {
            throw new ForbiddenException(ALREADY_REGISTERED);
        }

        userRepository.findSingleBy(new FindUserByLoginSpecification(loginUserToAdd))
                .orElseThrow(() -> new NotFoundException(USER_NOT_FOUND + loginUserToAdd));

        ChatType chatType = chat.getType();
        UserRole currentUserRole = membership.getUserRole();

        if (chatType.equals(ChatType.GROUP)) {
            if (!currentUserRole.equals(UserRole.ADMIN)) {
                throw new ForbiddenException(CANT_ADD_ANYONE_TO_GROUP_CHAT);
            }
        }

        if (chatType.equals(ChatType.PUBLIC)) {
            UserRole roleUserToAdd = UserRole.valueOf(membershipCreateDto.getUserRole());
            if (!currentUserRole.equals(UserRole.ADMIN) && roleUserToAdd.equals(UserRole.ADMIN)) {
                throw new ForbiddenException(USER_CANT_ADD_ADMIN);
            }
        }

        return saveMembership(membershipCreateDto, chat);
    }

    private MembershipDto registerToChat(MembershipCreateDto membershipCreateDto, Chat chat) {
        UserDto userDto = membershipCreateDto.getUser();
        String login = userDto.getLogin();
        if (isUserChatMember(chat.getId(), login)) {
            throw new ForbiddenException(ALREADY_REGISTERED);
        }
        ChatType chatType = chat.getType();
        if (chatType.equals(ChatType.GROUP)) {
            throw new ForbiddenException(CANT_REGISTER_IN_GROUP_CHAT);
        }
        UserRole userRole = UserRole.valueOf(membershipCreateDto.getUserRole());
        if (userRole.equals(UserRole.ADMIN)) {
            throw new ForbiddenException(CANT_REGISTER_AS_ADMIN);
        }
        return saveMembership(membershipCreateDto, chat);
    }

    private MembershipDto saveMembership(MembershipCreateDto membershipCreateDto, Chat chat) {
        Membership membershipToSave = modelMapper.map(membershipCreateDto, Membership.class);
        membershipToSave.setChat(chat);
        membershipRepository.transactionManage(EntityTransaction::begin);
        Membership createdMembership = membershipRepository.save(membershipToSave);
        membershipRepository.transactionManage(EntityTransaction::commit);
        return modelMapper.map(createdMembership, MembershipDto.class);
    }

}
