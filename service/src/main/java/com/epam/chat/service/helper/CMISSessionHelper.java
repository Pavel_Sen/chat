package com.epam.chat.service.helper;

import javax.naming.InitialContext;

import com.epam.chat.service.exception.CMISConnectionException;
import com.epam.chat.service.util.ReadProperties;
import org.apache.chemistry.opencmis.client.api.Session;
import org.apache.chemistry.opencmis.commons.exceptions.CmisObjectNotFoundException;

import com.sap.ecm.api.EcmService;
import com.sap.ecm.api.RepositoryOptions;
import com.sap.ecm.api.RepositoryOptions.Visibility;

public class CMISSessionHelper {

    private static final String LOOKUP_NAME = "java:comp/env/EcmService";
    private static final String UNIQUE_NAME_PROPERTY = "uniqueName";
    private static final String SECRET_KEY_PROPERTY = "secretKey";

    private static volatile CMISSessionHelper helper;

    private static Session openCmisSession = null;

    public static CMISSessionHelper getInstance() {
        if (helper == null) {
            synchronized (CMISSessionHelper.class) {
                if (helper == null) {
                    helper = new CMISSessionHelper();
                }
            }
        }
        return helper;
    }

    private CMISSessionHelper() {
        try {
            String repositoryUniqueName = ReadProperties.getInstance().getValue(UNIQUE_NAME_PROPERTY);
            String repositorySecretKey = ReadProperties.getInstance().getValue(SECRET_KEY_PROPERTY);
            InitialContext ctx = new InitialContext();
            EcmService ecmSvc = (EcmService) ctx.lookup(LOOKUP_NAME);
            try {
                openCmisSession = ecmSvc.connect(repositoryUniqueName, repositorySecretKey);
            } catch (CmisObjectNotFoundException e) {
                RepositoryOptions options = new RepositoryOptions();
                options.setUniqueName(repositoryUniqueName);
                options.setRepositoryKey(repositorySecretKey);
                options.setVisibility(Visibility.PROTECTED);
                ecmSvc.createRepository(options);
                openCmisSession = ecmSvc.connect(repositoryUniqueName, repositoryUniqueName);
            }
        } catch (Exception e) {
            throw new CMISConnectionException(e);
        }

    }

    public Session getSession() {
        return openCmisSession;
    }

}
