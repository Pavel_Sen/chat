package com.epam.chat.service;

import com.epam.chat.model.dto.MessageCreateOrUpdateDto;
import com.epam.chat.model.dto.response.MessageDto;

import java.util.List;

public interface MessageService {

    MessageDto findById(Long id);

    List<MessageDto> findAll();

    MessageDto saveMessage(Long chatId, String userLogin, MessageCreateOrUpdateDto message);

    MessageDto saveEvent(Long chatId, String text);

    MessageDto saveAttachment(Long chatId, String userLogin, String filename);

    MessageDto update(Long id, MessageCreateOrUpdateDto message, String userLogin);

    void deleteById(Long id, String userLogin);

    List<MessageDto> findMessagesByUserLogin(String login);

    List<MessageDto> findMessagesByUserLoginWithPagination(String login, Long limit, Integer offset);

    List<MessageDto> findMessagesByChatId(Long chatId);

    List<MessageDto> findMessagesByChatIdWithPagination(Long chatId, Long limit, Integer offset);

    Long getMessagesCountByChatId(Long id);

    Long getMessagesCountByUserLogin(String login);
}
