package com.epam.chat.service.impl;

import com.epam.chat.model.entity.User;
import com.epam.chat.repository.Repository;
import com.epam.chat.repository.specification.impl.user.FindUserByLoginSpecification;
import com.epam.chat.service.AuthService;

import javax.inject.Inject;
import java.util.Optional;

public class AuthServiceImpl implements AuthService {

    private Repository<User> userRepository;

    @Inject
    public AuthServiceImpl(Repository<User> userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public Optional<User> findByLogin(String login) {
        return userRepository.findSingleBy(new FindUserByLoginSpecification(login));
    }
}
