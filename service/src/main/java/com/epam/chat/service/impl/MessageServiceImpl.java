package com.epam.chat.service.impl;

import com.epam.chat.model.dto.MessageCreateOrUpdateDto;
import com.epam.chat.model.dto.response.MessageDto;
import com.epam.chat.model.entity.Chat;
import com.epam.chat.model.entity.Membership;
import com.epam.chat.model.entity.Message;
import com.epam.chat.model.entity.User;
import com.epam.chat.model.enums.ChatMode;
import com.epam.chat.model.enums.MessageType;
import com.epam.chat.model.enums.UserRole;
import com.epam.chat.repository.Repository;
import com.epam.chat.repository.specification.impl.chat.FindChatByIdSpecification;
import com.epam.chat.repository.specification.impl.membership.FindMembershipByUserLoginAndChatId;
import com.epam.chat.repository.specification.impl.message.FindAllMessageSpecification;
import com.epam.chat.repository.specification.impl.message.FindMessageByChatIdSpecification;
import com.epam.chat.repository.specification.impl.message.FindMessageByIdSpecification;
import com.epam.chat.repository.specification.impl.message.FindMessageByUserLoginSpecification;
import com.epam.chat.service.MessageService;
import org.modelmapper.ModelMapper;

import javax.inject.Inject;
import javax.persistence.EntityTransaction;
import javax.ws.rs.ForbiddenException;
import javax.ws.rs.NotFoundException;
import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class MessageServiceImpl implements MessageService {

    private static final String SYSTEM_USER_LOGIN = "SYSTEM";
    private static final String MESSAGE_NOT_FOUND = "Message is not found : ";
    private static final String CHAT_NOT_FOUND = "Chat is not found : ";
    private static final String FORBIDDEN_EXCEPTION = "Only chat members can add messages";
    private static final String MESSAGE_UPDATE_FORBIDDEN_EXCEPTION = "Only message author can update message";
    private static final String MESSAGE_DELETE_FORBIDDEN_EXCEPTION = "Only message author or chat admin can delete message";
    private static final String BROADCAST_MODE_FORBIDDEN_EXCEPTION = "Only admin can add message to chat in broadcast mode";

    private ModelMapper modelMapper = new ModelMapper();

    private Repository<Message> messageRepository;

    private Repository<Membership> membershipRepository;

    private Repository<Chat> chatRepository;

    @Inject
    public MessageServiceImpl(Repository<Message> messageRepository, Repository<Membership> membershipRepository,
                              Repository<Chat> chatRepository) {
        this.messageRepository = messageRepository;
        this.membershipRepository = membershipRepository;
        this.chatRepository = chatRepository;
    }

    public MessageDto findById(Long id) {
        Message message = messageRepository.findSingleBy(new FindMessageByIdSpecification(id))
                .orElseThrow(() -> new NotFoundException(MESSAGE_NOT_FOUND + id));
        return modelMapper.map(message, MessageDto.class);
    }

    public List<MessageDto> findAll() {
        List<Message> messages = messageRepository.findBy(new FindAllMessageSpecification());
        return messages.stream().map(message -> modelMapper.map(message, MessageDto.class)).collect(Collectors.toList());
    }

    public MessageDto saveMessage(Long chatId, String userLogin, MessageCreateOrUpdateDto messageDto) {
        Membership membership = membershipRepository.findSingleBy(new FindMembershipByUserLoginAndChatId(userLogin, chatId))
                .orElseThrow(() -> new ForbiddenException(FORBIDDEN_EXCEPTION));

        Chat chat = membership.getChat();
        if (chat.getMode().equals(ChatMode.BROADCAST) && !membership.getUserRole().equals(UserRole.ADMIN)) {
            throw new ForbiddenException(BROADCAST_MODE_FORBIDDEN_EXCEPTION);
        }

        Message messageToSave = modelMapper.map(messageDto, Message.class);
        messageToSave.setCreation(LocalDateTime.now());
        User user = membership.getUser();
        messageToSave.setUser(user);
        messageToSave.setChat(chat);
        messageToSave.setType(MessageType.MESSAGE);

        messageRepository.transactionManage(EntityTransaction::begin);
        Message message = messageRepository.save(messageToSave);
        messageRepository.transactionManage(EntityTransaction::commit);

        return modelMapper.map(message, MessageDto.class);
    }

    public MessageDto saveEvent(Long chatId, String text) {
        Chat chat = chatRepository.findSingleBy(new FindChatByIdSpecification(chatId))
                .orElseThrow(() -> new NotFoundException(CHAT_NOT_FOUND + chatId));

        Message messageToSave = new Message();
        messageToSave.setText(text);
        messageToSave.setChat(chat);
        messageToSave.setCreation(LocalDateTime.now());
        messageToSave.setType(MessageType.EVENT);
        User user = new User();
        user.setLogin(SYSTEM_USER_LOGIN);
        messageToSave.setUser(user);

        messageRepository.transactionManage(EntityTransaction::begin);
        Message message = messageRepository.save(messageToSave);
        messageRepository.transactionManage(EntityTransaction::commit);

        return modelMapper.map(message, MessageDto.class);
    }

    public MessageDto saveAttachment(Long chatId, String userLogin, String text) {
        Membership membership = membershipRepository.findSingleBy(new FindMembershipByUserLoginAndChatId(userLogin, chatId))
                .orElseThrow(() -> new ForbiddenException(FORBIDDEN_EXCEPTION));

        Chat chat = membership.getChat();
        if (chat.getMode().equals(ChatMode.BROADCAST) && !membership.getUserRole().equals(UserRole.ADMIN)) {
            throw new ForbiddenException(BROADCAST_MODE_FORBIDDEN_EXCEPTION);
        }

        Message messageToSave = new Message();
        messageToSave.setText(text);
        messageToSave.setChat(chat);
        messageToSave.setCreation(LocalDateTime.now());
        messageToSave.setType(MessageType.ATTACHMENT);
        User user = new User();
        user.setLogin(userLogin);
        messageToSave.setUser(user);

        messageRepository.transactionManage(EntityTransaction::begin);
        Message message = messageRepository.save(messageToSave);
        messageRepository.transactionManage(EntityTransaction::commit);

        return modelMapper.map(message, MessageDto.class);
    }

    public MessageDto update(Long id, MessageCreateOrUpdateDto messageDto, String userLogin) {
        Message message = messageRepository.findSingleBy(new FindMessageByIdSpecification(id))
                .orElseThrow(() -> new NotFoundException(MESSAGE_NOT_FOUND + id));
        if (!isCurrentUserMessageAuthor(message, userLogin)) {
            throw new ForbiddenException(MESSAGE_UPDATE_FORBIDDEN_EXCEPTION);
        }
        message.setText(messageDto.getText());

        messageRepository.transactionManage(EntityTransaction::begin);
        messageRepository.save(message);
        messageRepository.transactionManage(EntityTransaction::commit);

        return modelMapper.map(message, MessageDto.class);
    }

    public void deleteById(Long id, String userLogin) {
        Message message = messageRepository.findSingleBy(new FindMessageByIdSpecification(id))
                .orElseThrow(() -> new NotFoundException(MESSAGE_NOT_FOUND + id));
        if (!isCurrentUserChatAdmin(id, userLogin) && !isCurrentUserMessageAuthor(message, userLogin)) {
            throw new ForbiddenException(MESSAGE_DELETE_FORBIDDEN_EXCEPTION);
        }
        messageRepository.transactionManage(EntityTransaction::begin);
        messageRepository.removeById(id);
        messageRepository.transactionManage(EntityTransaction::commit);
    }

    @Override
    public List<MessageDto> findMessagesByUserLogin(String login) {
        return messageRepository.findBy(new FindMessageByUserLoginSpecification(login))
                .stream()
                .map(message -> modelMapper.map(message, MessageDto.class))
                .sorted(Comparator.comparing(MessageDto::getCreation))
                .collect(Collectors.toList());
    }

    @Override
    public List<MessageDto> findMessagesByUserLoginWithPagination(String login, Long limit, Integer offset) {
        return messageRepository.findByWithLimit(new FindMessageByUserLoginSpecification(login), limit, offset)
                .stream()
                .map(message -> modelMapper.map(message, MessageDto.class))
                .sorted(Comparator.comparing(MessageDto::getCreation))
                .collect(Collectors.toList());
    }

    @Override
    public List<MessageDto> findMessagesByChatId(Long chatId) {
        return messageRepository.findBy(new FindMessageByChatIdSpecification(chatId))
                .stream()
                .map(message -> modelMapper.map(message, MessageDto.class))
                .sorted(Comparator.comparing(MessageDto::getCreation))
                .collect(Collectors.toList());
    }

    @Override
    public List<MessageDto> findMessagesByChatIdWithPagination(Long chatId, Long limit, Integer offset) {
        return messageRepository.findByWithLimit(new FindMessageByChatIdSpecification(chatId), limit, offset)
                .stream()
                .map(message -> modelMapper.map(message, MessageDto.class))
                .sorted(Comparator.comparing(MessageDto::getCreation))
                .collect(Collectors.toList());
    }

    @Override
    public Long getMessagesCountByChatId(Long id) {
        return messageRepository.getFieldsCount(new FindMessageByChatIdSpecification(id));
    }

    @Override
    public Long getMessagesCountByUserLogin(String login) {
        return messageRepository.getFieldsCount(new FindMessageByUserLoginSpecification(login));
    }

    private boolean isCurrentUserMessageAuthor(Message message, String userLogin) {
        User user = message.getUser();
        String messageAuthor = user.getLogin();
        return messageAuthor.equals(userLogin);
    }

    private boolean isCurrentUserChatAdmin(Long id, String userLogin) {
        boolean result = false;
        Optional<Membership> currentUserMembership = membershipRepository.findSingleBy(new FindMembershipByUserLoginAndChatId(userLogin, id));
        if (currentUserMembership.isPresent()) {
            Membership membership = currentUserMembership.get();
            UserRole currentUserRole = membership.getUserRole();
            if (currentUserRole.equals(UserRole.ADMIN)) {
                result = true;
            }
        }
        return result;
    }
}
