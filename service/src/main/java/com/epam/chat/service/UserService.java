package com.epam.chat.service;

import com.epam.chat.model.dto.UserDto;

import java.util.List;

public interface UserService {

    UserDto findByLogin(String login);

    UserDto save(UserDto user);

    void delete(String login);

    List<UserDto> findAllUsers();
}
