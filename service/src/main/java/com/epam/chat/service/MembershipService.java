package com.epam.chat.service;

import com.epam.chat.model.dto.MembershipCreateDto;
import com.epam.chat.model.dto.response.MembershipDto;

import java.util.List;

public interface MembershipService {

    List<MembershipDto> findByChatId(Long chatId);

    List<MembershipDto> findByUserLogin(String login);

    MembershipDto findByChatIdAndUserLogin(Long id, String login);

    MembershipDto save(MembershipCreateDto membershipCreateDto, String currentUser, Long chatId);

    void removeByChatIdAndUserLogin(Long id, String login);

    boolean isUserChatMember(Long chatId, String login);
}
