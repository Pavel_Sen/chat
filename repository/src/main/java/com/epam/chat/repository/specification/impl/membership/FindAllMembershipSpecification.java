package com.epam.chat.repository.specification.impl.membership;

import com.epam.chat.model.entity.Membership;
import com.epam.chat.repository.specification.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

public class FindAllMembershipSpecification implements Specification<Membership> {
    @Override
    public Predicate toPredicate(Root<Membership> root, CriteriaBuilder criteriaBuilder) {
        return criteriaBuilder.and();
    }
}
