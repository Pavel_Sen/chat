package com.epam.chat.repository.specification.impl.chat;

import com.epam.chat.model.entity.Chat;
import com.epam.chat.repository.specification.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

public class FindAllChatSpecification implements Specification<Chat> {
    @Override
    public Predicate toPredicate(Root<Chat> criteriaQuery, CriteriaBuilder criteriaBuilder) {
        return criteriaBuilder.and();
    }
}
