package com.epam.chat.repository.specification;

import com.epam.chat.model.entity.BaseEntity;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

public interface Specification<T extends BaseEntity> {

    Predicate toPredicate(Root<T> root, CriteriaBuilder criteriaBuilder);
}
