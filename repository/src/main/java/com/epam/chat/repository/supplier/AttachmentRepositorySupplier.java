package com.epam.chat.repository.supplier;

import com.epam.chat.model.entity.Attachment;
import com.epam.chat.repository.Repository;
import com.epam.chat.repository.impl.RepositoryImpl;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.function.Supplier;

public class AttachmentRepositorySupplier implements Supplier<Repository<Attachment>> {

    private EntityManager entityManager;

    @Inject
    public AttachmentRepositorySupplier(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public Repository<Attachment> get() {
        return new RepositoryImpl<>(entityManager, Attachment.class);
    }
}
