package com.epam.chat.repository.supplier;

import com.epam.chat.model.entity.Membership;
import com.epam.chat.repository.Repository;
import com.epam.chat.repository.impl.RepositoryImpl;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.function.Supplier;

public class MembershipRepositorySupplier implements Supplier<Repository<Membership>> {

    private EntityManager entityManager;

    @Inject
    public MembershipRepositorySupplier(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public Repository<Membership> get() {
        return new RepositoryImpl<>(entityManager, Membership.class);
    }
}
