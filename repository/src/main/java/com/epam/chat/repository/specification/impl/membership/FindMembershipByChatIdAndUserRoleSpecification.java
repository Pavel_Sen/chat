package com.epam.chat.repository.specification.impl.membership;

import com.epam.chat.model.entity.Chat_;
import com.epam.chat.model.entity.Membership;
import com.epam.chat.model.entity.Membership_;
import com.epam.chat.model.enums.UserRole;
import com.epam.chat.repository.specification.Specification;
import lombok.AllArgsConstructor;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

@AllArgsConstructor
public class FindMembershipByChatIdAndUserRoleSpecification implements Specification<Membership> {

    private Long chatId;

    private UserRole userRole;

    @Override
    public Predicate toPredicate(Root<Membership> root, CriteriaBuilder criteriaBuilder) {
        return criteriaBuilder.and(criteriaBuilder.equal(root.get(Membership_.USER_ROLE), userRole),
                criteriaBuilder.equal(root.get(Membership_.CHAT).get(Chat_.ID), chatId));
    }
}
