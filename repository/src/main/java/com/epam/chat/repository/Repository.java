package com.epam.chat.repository;

import com.epam.chat.model.entity.BaseEntity;
import com.epam.chat.model.entity.Message;
import com.epam.chat.repository.specification.Specification;

import javax.persistence.EntityTransaction;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;

public interface Repository<T extends BaseEntity> {

    T save(T item);

    List<T> save(List<T> items);

    void removeById(Long id);

    void remove(T item);

    void remove(List<T> items);

    List<T> findBy(Specification<T> specification);

    List<T> findByWithLimit(Specification<T> specification, Long limit, Integer offset);

    Optional<T> findSingleBy(Specification<T> specification);

    void transactionManage(Consumer<EntityTransaction> consumer);

    Long getFieldsCount(Specification<T> specification);
}
