package com.epam.chat.repository.supplier;

import com.epam.chat.model.entity.Message;
import com.epam.chat.repository.Repository;
import com.epam.chat.repository.impl.RepositoryImpl;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.function.Supplier;

public class MessageRepositorySupplier implements Supplier<Repository<Message>> {

    private final EntityManager entityManager;

    @Inject
    public MessageRepositorySupplier(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public Repository<Message> get() {
        return new RepositoryImpl<>(entityManager, Message.class);
    }
}
