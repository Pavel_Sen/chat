package com.epam.chat.repository.supplier;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.function.Supplier;

public class EntityManagerFactorySupplier implements Supplier<EntityManagerFactory> {

    private static final String PERSISTENCE_UNIT_NAME = "production";

    private final EntityManagerFactory entityManagerFactory;

    public EntityManagerFactorySupplier() {
        this.entityManagerFactory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
    }

    @Override
    public EntityManagerFactory get() {
        return entityManagerFactory;
    }
}
