package com.epam.chat.repository.specification.impl.message;

import com.epam.chat.model.entity.Message;
import com.epam.chat.model.entity.Message_;
import com.epam.chat.repository.specification.Specification;
import lombok.AllArgsConstructor;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

@AllArgsConstructor
public class FindMessageByIdSpecification implements Specification<Message> {

    private Long id;

    @Override
    public Predicate toPredicate(Root<Message> root, CriteriaBuilder criteriaBuilder) {
        return criteriaBuilder.equal(root.get(Message_.ID), id);
    }
}
