package com.epam.chat.repository.specification.impl.attachment;

import com.epam.chat.model.entity.Attachment;
import com.epam.chat.model.entity.Attachment_;
import com.epam.chat.model.entity.Chat_;
import com.epam.chat.model.entity.Message_;
import com.epam.chat.repository.specification.Specification;
import lombok.AllArgsConstructor;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

@AllArgsConstructor
public class AttachmentByChatIdSpecification implements Specification<Attachment> {

    private long chatId;

    @Override
    public Predicate toPredicate(Root<Attachment> root, CriteriaBuilder criteriaBuilder) {
        return criteriaBuilder.and(
                criteriaBuilder.equal(root.get(Attachment_.MESSAGE).get(Message_.CHAT).get(Chat_.ID), chatId));
    }

}
