package com.epam.chat.repository.specification.impl.membership;

import com.epam.chat.model.entity.Chat_;
import com.epam.chat.model.entity.Membership;
import com.epam.chat.model.entity.Membership_;
import com.epam.chat.repository.specification.Specification;
import lombok.AllArgsConstructor;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

@AllArgsConstructor
public class FindMembershipByChatIdSpecification implements Specification<Membership> {

    private Long chatId;

    @Override
    public Predicate toPredicate(Root<Membership> root, CriteriaBuilder criteriaBuilder) {
        return criteriaBuilder.and(criteriaBuilder.and(criteriaBuilder.equal(root.get(Membership_.CHAT).get(Chat_.ID), chatId)));
    }
}
