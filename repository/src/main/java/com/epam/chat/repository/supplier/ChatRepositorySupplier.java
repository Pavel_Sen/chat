package com.epam.chat.repository.supplier;

import com.epam.chat.model.entity.Chat;
import com.epam.chat.repository.Repository;
import com.epam.chat.repository.impl.RepositoryImpl;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.function.Supplier;

public class ChatRepositorySupplier implements Supplier<Repository<Chat>> {

    private EntityManager entityManager;

    @Inject
    public ChatRepositorySupplier(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public Repository<Chat> get() {
        return new RepositoryImpl<>(entityManager, Chat.class);
    }
}
