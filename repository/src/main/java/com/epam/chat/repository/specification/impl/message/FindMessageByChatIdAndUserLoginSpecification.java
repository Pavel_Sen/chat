package com.epam.chat.repository.specification.impl.message;

import com.epam.chat.model.entity.Chat_;
import com.epam.chat.model.entity.Message;
import com.epam.chat.model.entity.Message_;
import com.epam.chat.model.entity.User_;
import com.epam.chat.repository.specification.Specification;
import lombok.AllArgsConstructor;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

@AllArgsConstructor
public class FindMessageByChatIdAndUserLoginSpecification implements Specification<Message> {

    private String login;
    private Long chatId;

    @Override
    public Predicate toPredicate(Root<Message> root, CriteriaBuilder criteriaBuilder) {
        return criteriaBuilder.and(criteriaBuilder.equal(root.get(Message_.CHAT).get(Chat_.ID), chatId),
                criteriaBuilder.equal(root.get(Message_.USER).get(User_.LOGIN), login));
    }
}
