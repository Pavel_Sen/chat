package com.epam.chat.repository.specification.impl.user;

import com.epam.chat.model.entity.User;
import com.epam.chat.repository.specification.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

public class AllUsersSpecification implements Specification<User> {

    @Override
    public Predicate toPredicate(Root<User> root, CriteriaBuilder criteriaBuilder) {
        return criteriaBuilder.and();
    }
}
